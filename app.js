var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var requestHandlers = require("./requestHandlers");
var cors = require('cors')
var routes = require('./routes/index');
var users = require('./routes/users');
var util = require('util');
var url = require('url');

var app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var router = express.Router();              // get an instance of the express Router


app.options('*', cors({origin: "*"}));

// middleware to use for all requests
router.use(function(req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  console.log('Something is happening.'+req.url);
  next(); // make sure we go to the next routes and don't stop here
});

router.get('/', function(req, res) {
  res.json({ message: 'hooray! welcome to our api!' });
});

router.post('/login', function(req, res, next) {
  requestHandlers.loginAndSetCookie(req,res);
});

router.get('/login', function(req, res, next) {
  requestHandlers.loginAndSetCookie(req,res);
});

router.get('/getCaseNum', function(req, res, next) {
  requestHandlers.getCaseNum(req,res);
});

router.get('/getCookie', function(req, res, next) {
  requestHandlers.getCookie(req,res);
});

//getQueryPage
router.post('/getMainQuery', function(req, res, next) {
  requestHandlers.getMainQuery(req,res);
});

router.get('/getAttorney', function(req, res, next) {
  requestHandlers.getAttorney(req,res);
});

router.get('/getParty', function(req, res, next) {
  requestHandlers.getParty(req,res);
});

router.get('/getCaseSummary', function(req, res, next) {
  requestHandlers.getCaseSummary(req,res);
});

router.get('/getFilers', function(req, res, next) {
  requestHandlers.getFilers(req,res);
});
router.get('/getDocketReport', function(req, res, next) {
  requestHandlers.getDocketReport(req,res);
});

router.post('/getDocketReport', function(req, res, next) {
  requestHandlers.getDocketReport(req,res);
});

router.post('/getReport', function(req, res, next) {
  requestHandlers.getReport(req,res);
});

router.get('/getReport', function(req, res, next) {
  requestHandlers.getReport(req,res);
});

router.get('/getCriminalCaseReport', function(req, res, next) {
  requestHandlers.getCriminalCaseReport(req,res);
});

router.get('/getDocketActivityReport', function(req, res, next) {
  requestHandlers.getDocketActivityReport(req,res);
});

router.post('/getPersons', function(req, res, next) {
  console.log ("/getPersons called");
  requestHandlers.getPersons(req,res);
});

router.get('/getFormAction', function(req, res, next) {
  console.log ("/getFormAction called");
  requestHandlers.getFormAction(req,res);
});

router.get('/getCivilCaseReportQuery', function(req, res, next) {
  console.log ("/getCivilCaseReportQuery called");
  requestHandlers.getCivilCaseReportQuery(req,res);
});

router.get('/getCriminalCaseReportQuery', function(req, res, next) {
  console.log ("/getCriminalCaseReportQuery called");
  requestHandlers.getCriminalCaseReportQuery(req,res);
});


router.get('/getCalendarEventsReportQuery', function(req, res, next) {
  console.log ("/getCalendarEventsReportQuery called");
  requestHandlers.getCalendarEventsReportQuery(req,res);
});

router.get('/getDocketActivityReportQuery', function(req, res, next) {
  console.log ("/getDocketActivityReportQuery called");
  requestHandlers.getDocketActivityReportQuery(req,res);
});

router.get('/getWrittenOptionsReportQuery', function(req, res, next) {
  console.log ("/getWrittenOptionsReportQuery called");
  requestHandlers.getWrittenOptionsReportQuery(req,res);
});

router.get('/getJudgmentIndexReportFormQuery', function(req, res, next) {
  console.log ("/getJudgmentIndexReportQuery called");
  requestHandlers.getJudgmentIndexReportQuery(req,res);
});


router.get('/getCommonFormData', function(req, res, next) {
  console.log ("/getCommonFormData called");
  requestHandlers.getCommonFormData(req,res);
});

router.get('/getCommonMainData', function(req, res, next) {
  console.log ("/getCommonMainData called");
  requestHandlers.getMainData(req,res);
});




router.get('/query1', function(req, res, next) {
  res.json({ message: 'query1! welcome to our api!' });
});
router.get('/query2', function(req, res, next) {
  res.json({ message: 'query2! welcome to our api!' });
});
router.get('/query3', function(req, res, next) {
  res.json({ message: 'query3! welcome to our api!' });
});
app.use('/', router);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
