var querystring = require("querystring");
var fs = require("fs");
var formidable = require("formidable");
var http = require('http');
var request = require('request');
var util = require("util");
var express = require('express'); // bring in the the express api
var fs = require('fs'); // bring in the file system api
var url = require('url');
var cheerio = require('cheerio');
var cookie = "";
var util = require('util');

function getCookie(req, res) {
    res.json({cookie: cookie});
};

function loginAndSetCookie(inreq, inres) {

    console.log ("loginAndSetCookie");

    console.log("body:"+util.inspect(inreq.body, false, null));
    console.log ("username="+inreq.body.login);
    console.log ("password="+inreq.body.key);

    var login = inreq.body.login;
    var key = inreq.body.key;
    var button = inreq.body.button;

    var loginform = {
        login: login, //'tr1234',
        key: key,//'Pass!234',
        button1: button //'Login'
    };

    var loginform = {
        login: 'tr1234',
        key: 'Pass!234',
        button1: 'Login'
    };

    var formData = querystring.stringify(loginform);
    var contentLength = formData.length;
    request({
        headers: {
            'Content-Length': contentLength,
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        uri: 'https://dcecf.psc.uscourts.gov/cgi-bin/login.pl?logout',
        body: formData,
        method: 'POST'
    }, function (err, res, body) {
        ex = /document.cookie="(.)*"/g;
        array = ex.exec(body);
        console.log("body=" + body);
        if (array == null) {
            console.log("Noo cookie found");
            inres.json({response: false});
        } else{
            cookie = array[0];
            console.log("get cookie:" + cookie);
            a = cookie.split("=");
            cookie = "PacerSession=" + a[2] + ";PacerPref=receipt=Y";
            console.log("get cookie:" + cookie);

            inres.json({response: true});
        }
    },
    function () {
        inres.json({response:false});
    });
}

function getCaseNum(inreq, inres) {

    var perlScript = inreq.url.substr("/getCaseNum".length);
    var fullUrl = 'https://dcecf.psc.uscourts.gov/cgi-bin/iquery.pl'+perlScript;
    console.log ("sending :"+fullUrl)

    request({
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Cookie': cookie
            },
            uri: fullUrl,
            method: 'GET'
        }, function (err, res, body) {
            console.log ("body="+body);
            var ex = /document.cookie[\t ]*=[\t ]*"(.)*"/g;
            var array = ex.exec(body);
            var cookie = array[0];
            var arr = cookie.split("\"");
            console.log ("case num:"+arr[1]);

            inres.json({caseNum: arr[1]});

        },
        function () {
            inres.json({caseNum:""});
        });
}

function trimLine(s) {
    return s.replace(/\n|\r/g, "");
}


function getMainQuery(inreq, inres) {
    request({
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Cookie': cookie
        },
        uri: 'https://dcecf.psc.uscourts.gov/cgi-bin/iquery.pl',
        method: 'POST'
    }, function (err, res, body) {
        var options1 = [];
        var options2 = [];
        var $ = cheerio.load(body);

        $("[name='nature_suit'] option").each(function (index) {
            options1.push($(this).text().trim());
        });
        $("[name='cause_action'] option").each(function (index) {
            options2.push($(this).text().trim());
        });
        inres.json({nature_suit: options1, cause_action: options2});
    });
}

function getCriminalCaseReportQuery(inreq, inres){
    var url = 'https://dcecf.psc.uscourts.gov/cgi-bin/CrCaseFiled-Rpt.pl';
    getFormOptions(inreq,inres,url,'CriminalCaseReport');
}

function getCivilCaseReportQuery(inreq, inres){
    var url = 'https://dcecf.psc.uscourts.gov/cgi-bin/CaseFiled-Rpt.pl';
    getFormOptions(inreq,inres,url,'CivilCaseReport');
}

function getCalendarEventsReportQuery(inreq, inres){
    var url = 'https://dcecf.psc.uscourts.gov/cgi-bin/CalEvents.pl';
    getFormOptions(inreq,inres,url,'CalendarEventsReport');
}

function getDocketActivityReportQuery(inreq, inres){
    var url = 'https://dcecf.psc.uscourts.gov/cgi-bin/DktActivityRpt.pl';
    getFormOptions(inreq,inres,url,'DocketActivityReport');
}

function getJudgmentIndexReportQuery(inreq, inres){
    var url = 'https://dcecf.psc.uscourts.gov/cgi-bin/JdgIdxRpt.pl';
    getFormOptions(inreq,inres,url,'JudgmentIndexReport');
}


function getWrittenOptionsReportQuery(inreq, inres){
    var url = 'https://dcecf.psc.uscourts.gov/cgi-bin/WrtOpRpt.pl';
    getFormOptions(inreq,inres,url,'WrittenOptionsReport');
}

function getReport(inreq, inres) {

    var urlScript = "https://dcecf.psc.uscourts.gov/cgi-bin/";

    var queryData = url.parse(inreq.url, true).query;
    if (queryData.actionPath) {
        urlScript = urlScript + queryData.actionPath;
    }

    var reportName = inreq.body['reportName'];

    var formParams = {};
    for (var key in inreq.body){
        var elemVal= inreq.body[key];
        if (elemVal == true )
            formParams[key] = "on";
        else {
                console.log ("elemval:"+elemVal+":"+key);
                formParams[key] = elemVal;
        }
    }
    delete formParams["reportName"];
    delete formParams["action"];
    var formData = querystring.stringify(formParams);
    var contentLength = formData.length;

    getCommonReport(inreq,inres,urlScript,reportName,reportName+"Details",formData,contentLength);
}

function getCommonFormData (req, res)
{
    console.log ("/getCommonFormData called");
    var queryData = url.parse(req.url, true).query;
    var action = queryData.action;
    var caseID = queryData.caseID;
    var basePath = "https://dcecf.psc.uscourts.gov/cgi-bin/";
    var urlVal = "";
    if (action == "MainForm")
        urlVal = basePath+'iquery.pl?'+caseID;
    else if (action == "CaseFileLocation")
        urlVal = basePath+'QryRMSLocation.pl?'+caseID;
    else if (action == "DeadlinesHearings")
        urlVal = basePath+'SchedQry.pl?'+caseID;
    else if (action == "DocketReport")
        urlVal = basePath+'DktRpt.pl?'+caseID;
    else if (action == "HistoryDocuments")
        urlVal = basePath+'HistDocQry.pl?'+caseID;
    else if (action == "RelatedTransactions")
        urlVal = basePath+'RelTransactQry.pl?'+caseID;
    else if (action == "ViewDocument")
        urlVal = basePath+'qryDocument.pl?'+caseID;

    console.log ("Executing "+urlVal+" in getCommonFormData");
    getFormOptions(req,res,urlVal,action);
}

function getMainData(req, res) {

    var queryData = url.parse(req.url, true).query;
    var basePath = "https://dcecf.psc.uscourts.gov/cgi-bin/";
    var urlVal = "";
    var action = queryData.action;
    if ( action == "Alias")
        urlVal = basePath+'qryAlias.pl?';
    else if (action == "AssociatedCases")
        urlVal = basePath+'qryAscCases.pl?';
    else if (action == "Attorney")
        urlVal = basePath+'qryAttorneys.pl?';
    else if (action == "CaseSummary")
        urlVal = basePath+'qrySummary.pl?';
    else if (action == "Filers")
        urlVal = basePath+'FilerQry.pl?';
    else if (action == "Party")
        urlVal = basePath+'qryParties.pl?';
    else if (action == "Status")
        urlVal = basePath+'StatusQry.pl?';

    console.log ("Executing "+urlVal+" in getMainData");
    if (action == "Filers")
        getCommnonPageContent2 (req,res,urlVal,"componentData","componentDataDetails",action);
    else
        getCommnonPageContent (req,res,urlVal,"componentData","componentDataDetails",action);
}

function getFormOptions(inreq, inres,url,reportName) {
    request({
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Cookie': cookie
        },
        uri: url,
        method: 'GET'
    }, function (err, res, body) {
        var $ = cheerio.load(body);

        console.log($('html').html());

        var options = {};
        var form = {};
        form.optionFields = [];
        form.otherFields = [];
        form['reportName'] = reportName;

        $('select').each(function(){
            var fieldName = $(this).attr('name');
            options[fieldName] = [];
            console.log (fieldName);

            var labelName = $(this).parent().prev().text();
            labelName = labelName.trim();
            if (labelName.length == 0) {
                labelName = $(this).parent().find("b:first-child").text();
                if (labelName.trim().length == 0)
                    labelName = $(this).parent()[0].previousSibling.data
            }
            labelName = labelName.trim();


            if (labelName.length > 0 && labelName.length < 30)
                    form['optionFields'].push({fieldName:fieldName,fieldLabel:labelName,type:"select"});
            else {
                    var arlen = form['optionFields'].length;
                    var lastLabel = form['optionFields'][arlen-1];
                    var match = null;
                    if (lastLabel && lastLabel.length > 0)
                        match = lastLabel.match(/\d+/);
                    if (match == null){
                        lastLabel = lastLabel+" 1";
                    }else{
                        lastLabel = lastLabel.replace(/\d+/,"");
                        lastLabel = lastLabel+(parseInt(match[0]) + 1 );
                    }
                    if (form['optionFields'].indexOf(lastLabel) < 0)
                        form['optionFields'].push({fieldName:fieldName,fieldLabel:labelName,type:"select"});
            }

        });



        $('select').find('option').each (function()
        {
               var arName = $(this).parent().attr('name');
               var opval  = $(this).attr("value");
               var optext = $(this).text().trim();
               options[arName].push([opval,optext]);
        });



        $('input[type="text"]').not('input[onchange *= "CheckDate"],input[onchange *= "VerifyDate"]').each (function()
        {
                var fieldName = $(this).attr("name");
                var fieldValue = $(this).attr("value");
                var type = $(this).attr("type");
                var labelName = $(this).parent().prev().text();
                if (labelName.length == 0)
                    labelName = $(this).parent()[0].previousSibling.data;
                if (labelName)
                    labelName = labelName.trim();
                if (!labelName){
                    var id = $(this).attr("id");
                    if (id.trim().indexOf ("case_number_text_area") == 0) {
                        labelName = "Case Number";
                        fieldName = "case_num";
                    }
                }

                type = type.toLowerCase();
                if (form['otherFields'].indexOf(labelName) < 0 &&  (labelName && labelName.length < 40)) {
                    form['otherFields'].push({fieldLabel: labelName, fieldName:fieldName,fieldValue:fieldValue,type: type});
                    console.log (labelName+" TX :"+type);
                }
        });

        $('input[type="hidden"],input[type="HIDDEN"]').not('input[onchange *= "CheckDate"],input[onchange *= "VerifyDate"]').each (function()
        {
            var fieldName = $(this).attr("name");
            var fieldValue = $(this).attr("value");
            var type = $(this).attr("type");
            var labelName = "";
            if (typeof fieldValue == 'undefined')
                fieldValue = "";
            if (typeof fieldName == 'undefined') {
                fieldName = $(this).attr("id");
            }
            type = type.toLowerCase();
            form['otherFields'].push({fieldLabel: labelName, fieldName:fieldName,fieldValue:fieldValue,type: type});
            console.log (fieldName+" HIDDEN :"+fieldValue);
        });

        $('input[onchange *= "CheckDate"],input[onchange *= "VerifyDate"]').each (function()
        {
            var type = "date";
            var fieldValue = $(this).attr("value");
            var fieldName = $(this).attr("name");
            var onchange = $(this).attr("onchange");
            var labelName = $(this).prev().contents(":not(:empty)").first().text();

            if (labelName.length == 0) {
                labelName = $(this).parent().prev().text();
                if (labelName.length == 0)
                    labelName = $(this).parent()[0].previousSibling.data;
                if (labelName.trim().length == 0)
                    labelName = $(this).parent().text();
            }
            labelName = labelName.trim();
            if (onchange.indexOf("CheckTerm") >= 0)
                type = "text";

            type = type.toLowerCase();
            if (type != 'hidden' && type != 'reset' && type != "button" &&
                (labelName && labelName.length < 30)) {
                form['otherFields'].push({fieldName: fieldName,fieldLabel:labelName, fieldValue:fieldValue,type: type});
                console.log (labelName+" DT :"+fieldName+":"+type);
            }
        });


        $('input[type="radio"],input[type="RADIO"]').each (function()
        {
                var checked = $(this).attr("checked");
                var type = $(this).attr("type");
                var fieldValue = $(this).attr("value");
                var fieldName = $(this).attr("name");
                var labelName = $(this).next().contents(":not(:empty)").first().text();
                if (checked)
                    fieldValue = true;
                else
                    fieldValue = false;
                type = type.toLowerCase();
                if (labelName && labelName.length < 30) {
                    form['otherFields'].push({fieldName: fieldName,fieldLabel:labelName, fieldValue:fieldValue,type: type});
                    console.log (labelName+" CB :"+type);
                }
        });

        $('input[type="checkbox"],input[type="CHECKBOX"]').each (function()
        {
                var checked = $(this).attr("checked");
                var type = $(this).attr("type");
                var fieldValue = $(this).attr("value");
                var fieldName = $(this).attr("name");
                var labelName = $(this).next().contents(":not(:empty)").first().text();
                if (labelName.length == 0) {
                    var sibIndex = $(this).index()+1;
                    labelName = $(this)[0].nextSibling.nodeValue;
                    if (labelName == null || labelName.trim().length == 0){
                        labelName = $(this).parent().next().find('b:nth-child('+sibIndex+')').text();
                    }
                }
                labelName = labelName.trim();

                if (checked)
                    fieldValue = true;
                else
                    fieldValue = false;

                type = type.toLowerCase();
                if (labelName && labelName.length < 30) {
                    form['otherFields'].push({fieldName: fieldName,fieldLabel:labelName, fieldValue:fieldValue, type: type});
                    console.log (labelName+" CB :"+type);
                }
        });


        var action = $("form").first().attr("action");
        var ar  = action.split("/");
        action = ar[ar.length-1];
        console.log("Form action path:"+action);
        form["action"] = action;

        inres.json({options:options,form:form});
    });
}


function getBilling ($)
{
    bill = [];
    $("center").last().find("table tr").each(function(index){
        trdata = []
        tro = $(this);
        tro.contents().each(function(index){
            val = $(this).text().trim();
            if (val != '')
                trdata.push(val);
        });
        if (trdata.length > 0)
            bill.push(trdata);
    });
    return bill;
}

function getPersons(inreq, inres) {
    var formParams = {
        all_case_ids: "0",
        case_num: "",
        filed_from : "",
        filed_to : "",
        lastentry_from : "",
        lastentry_to : "",
        last_name: "", //"Smith",
        first_name: "",
        middle_name: "",
        person_type: ""
    };
    for (var key in inreq.body){
        formParams[key] = inreq.body[key];
    }

    console.log ("received in getPersons:"+formParams);

    var formData = querystring.stringify(formParams);
    var contentLength = formData.length;

    var url = 'https://dcecf.psc.uscourts.gov/cgi-bin/iquery.pl?428225598587965-L_15_0-1';
    getPersonsContent (inreq,inres,url,"persons","personsDetails",formData,contentLength);
}

function getFormAction(inreq, inres) {
    var url = 'https://dcecf.psc.uscourts.gov/cgi-bin/';
    getFormActionExecute(inreq,inres,url);
}

function getAttorney(inreq, inres) {
    var url = 'https://dcecf.psc.uscourts.gov/cgi-bin/qryAttorneys.pl?';
    getCommnonPageContent (inreq,inres,url,"attorney","attorneyDetails");
}

function getParty(inreq, inres) {
    var url = 'https://dcecf.psc.uscourts.gov/cgi-bin/qryParties.pl?';
    getCommnonPageContent (inreq,inres,url,"party","partyDetails");
}

function getCaseSummary(inreq, inres) {
    var url = 'https://dcecf.psc.uscourts.gov/cgi-bin/qrySummary.pl?';
    //getCommnonPageContent (inreq,inres,url,"casesummary","casesummaryDetails");
    var reportName = "CaseSummary";
    getCommonReport(inreq,inres,url,reportName,reportName+"Details");
}

function getFilers(inreq, inres) {
    var url = 'https://dcecf.psc.uscourts.gov/cgi-bin/FilerQry.pl?';
    getCommnonPageContent2 (inreq,inres,url,"","filers","filersDetails");
}

function getCriminalCaseReport(inreq, inres) {
    var formParams = {
        office: "1",
        case_type: "cr",
        case_flags: "",
        citation: "",
        pending_citations: "1",
        filed_from : "3/19/2005",
        filed_to : "3/26/2016",
        terminal_digit: "",
        pending_defendants: "on",
        fugitive_defendants: "",
        nonfugitive_defendants: "1",
        sort1: "case number",
        sort2: "",
        sort3: "",
        format: "standard"
    };
    var formData = querystring.stringify(formParams);
    var contentLength = formData.length;
    var url = 'https://dcecf.psc.uscourts.gov/cgi-bin/CrCaseFiled-Rpt.pl?202193967482791-L_764_0-1?';
    getCommnonPageContent (inreq,inres,url,"docketReport","docketReportDetails","",formData,contentLength);
}


function getDocketReport(inreq, inres) {

    var urlScript = "https://dcecf.psc.uscourts.gov/cgi-bin/";

    var queryData = url.parse(inreq.url, true).query;
    if (queryData.actionPath) {
        urlScript = urlScript + queryData.actionPath;
    }

    var formParams = {
        view_comb_doc_text: "",
        //all_case_ids: "63443",
        //CaseNum_63443: "on",
        date_range_type: "Filed",
        date_from:"",
        date_to:"",
        documents_numbered_from_:"",
        documents_numbered_to_:"",
        list_of_parties_and_counsel:"on",
        terminated_parties:"on",
        output_format:"html",
        PreResetField:"",
        PreResetFields:"",
        sort1:"oldest date first"
    };
    for (var key in inreq.body){
        var elemVal= inreq.body[key];
        if (elemVal == true )
            formParams[key] = "on";
        else
            formParams[key] = elemVal;
    }
    var formData = querystring.stringify(formParams);
    var contentLength = formData.length;

    getCommonReport(inreq,inres,urlScript,"docketReport","docketReportDetails",formData,contentLength);
}

function getDocketActivityReport(inreq, inres) {
    var formParams = {
        all_case_ids: "0",
        case_num: "",
        office: "1",
        case_type: "cv",
        event_category: "",
        case_flags: "",
        filed_from : "3/19/2005",
        filed_to : "3/26/2016",
        date_range_limit: "",
        docket_text: "",
        sort1: "case number",
        sort2: "",
    };
    var formData = querystring.stringify(formParams);
    var contentLength = formData.length;
    var url = 'https://dcecf.psc.uscourts.gov/cgi-bin/DktActivityRpt.pl?591401268444996-L_914_0-1?';
    getCommonReport(inreq,inres,url,"docketReport","docketReportDetails",formData,contentLength);
    //getCommnonPageContent (inreq,inres,url,"docketReport","docketReportDetails",formData,contentLength);
}

function getCommnonPageContent(inreq, inres, urlScript,pageName,pageDetails,componentName,formData,contentLength) {

    var queryData = url.parse(inreq.url, true).query;
    urlScript = urlScript+queryData.caseID;

    var urlMethod = "GET";
    if (formData){
        urlMethod = "POST";
    }
    console.log ("getCommnonPageContent entered");
    request({
        headers: {
            'Content-Length': contentLength,
            'Content-Type': 'application/x-www-form-urlencoded',
            'Cookie': cookie
        },
        uri: urlScript,
        method: urlMethod,
        body: formData
    }, function (err, res, body) {
        var options1 = [];
        var options2 = [];
        console.log("url:"+url+" err:"+err);
        //console.log("body:"+body);
        var $ = cheerio.load(body);

        var table = [];
        var pageHeading = [];

        $("center").first().contents().each(function (index) {
            val = $(this).text().trim();
            if (val != '')
                pageHeading.push(val);
        });

        $("td[valign='top'],tr[valign='top']").each(function (index) {
            var tds = [];
            var tdo = $(this);
            tdo.contents().each(function (index) {
                var href = $(this).find("a").attr("href");
                if (href != '' && href != null){
                    tds.push(href);
                }

                var val = $(this).text().trim();
                if (val != '')
                    tds.push(val)
            });

            var repre = $(this).next().next();
            var r = [];
            repre.contents().each(function (index) {
                val = $(this).text().trim();
                if (val != '')
                    r.push(val);
            });
            tds.splice(0, 0, r);
            table.push(tds);

        });
        var billing = getBilling($);

        var returnDoc = {};
        returnDoc[pageName] = pageHeading;
        returnDoc[pageDetails] = table;
        returnDoc['billing'] = billing;
        returnDoc['componentName'] = componentName;

        inres.json(returnDoc);
        });
    }

function getCommnonPageContent2(inreq, inres, urlScript,pageName,pageDetails,componentName,formData,contentLength) {

    var queryData = url.parse(inreq.url, true).query;
    urlScript = urlScript+queryData.caseID;

    var urlMethod = "GET";
    if (formData){
        urlMethod = "POST";
    }
    console.log ("getCommnonPageContent entered");
    request({
        headers: {
            'Content-Length': contentLength,
            'Content-Type': 'application/x-www-form-urlencoded',
            'Cookie': cookie
        },
        uri: urlScript,
        method: urlMethod,
        body: formData
    }, function (err, res, body) {
        var options1 = [];
        var options2 = [];
        console.log("url:"+url+" err:"+err);
        //console.log("body:"+body);
        var $ = cheerio.load(body);

        var table = [];
        var pageHeading = [];

        $("center").first().contents().each(function (index) {
            val = $(this).text().trim();
            if (val != '')
                pageHeading.push(val);
        });

        $("table").first().find("tr").each(function (index) {
            var tds = [];
            var tdo = $(this);
            tdo.contents().each(function (index) {
                var href = $(this).find("a").attr("href");
                if (href != '' && href != null){
                    tds.push(href);
                }

                var val = $(this).text().trim();
                if (val != '')
                    tds.push(val)
            });

            var repre = $(this).next().next();
            var r = [];
            repre.contents().each(function (index) {
                val = $(this).text().trim();
                if (val != '')
                    r.push(val);
            });
            tds.splice(0, 0, r);
            table.push(tds);

        });
        var billing = getBilling($);

        var returnDoc = {};
        returnDoc[pageName] = pageHeading;
        returnDoc[pageDetails] = table;
        returnDoc['billing'] = billing;
        returnDoc['componentName'] = componentName;

        inres.json(returnDoc);
    });
}

function getCommonReport(inreq, inres, urlScript,pageName,pageDetails,formData,contentLength) {

    var queryData = url.parse(inreq.url, true).query;
    if (queryData.caseID) {
        urlScript = urlScript + queryData.caseID;
    }
    console.log ("getCommonReport:URL:"+urlScript)
    var urlMethod = "GET";
    if (formData){
        urlMethod = "POST";
    }
    var table = [];
    console.log ("getCommnonPageContent entered");
    request({
        headers: {
            'Content-Length': contentLength,
            'Content-Type': 'application/x-www-form-urlencoded',
            'Cookie': "_baga=GA1.2.1281256340.1460751926;"+cookie
        },
        uri: urlScript,
        method: urlMethod,
        body: formData
    }, function (err, res, body) {
        var options1 = [];
        var options2 = [];
        console.log("url:" + url + " err:" + err);//+":"+body);
        var $ = cheerio.load(body);

        var pageHeading = [];
        $("h2,h3,h4").contents().each(function (index) {
            var val = $(this).text().trim();
            if (val != '')
                pageHeading.push(val);
        });

        $("table").each(function (index) {
            var tableObj = $(this);
            var rows = [];
            var parentName = $(this).parent()[0].name.toLowerCase();
            if (parentName != "td") {
                tableObj.children("tr").each(function (index) {
                    var columns = [];
                    var rowObj = $(this);
                    rowObj.children("td,th").each(function (index) {
                        var tds = [];
                        var tdObj = $(this);
                        tdObj.contents().each(function (index) {
                            var nodeName = $(this)[0].name;
                            if (nodeName !== undefined)
                                nodeName = nodeName.toLowerCase();
                            if (nodeName == 'a') {
                                var href = $(this).attr("href");
                                if (href != '' && href != null)
                                    tds.push(href);
                            }
                            if (nodeName != 'script') {
                                var val = $(this).text().trim();
                                if (val != '')
                                    tds.push(val);
                                else if ($(this)[0].type != 'text') {
                                    tds.push("<br/>");
                                }
                            }
                        });
                        columns.push(tds);
                    });
                    rows.push(columns);
                });
                table.push(rows);
            }
        });
        inres.json({heading:pageHeading,contents:table});
    });


};

function getFormActionExecute(inreq, inres, urlScript) {

    var queryData = url.parse(inreq.url, true).query;
    if (queryData.actionPath) {
        urlScript = urlScript + queryData.actionPath;
    }
    if (queryData.caseID) {
        urlScript = urlScript +"?"+ queryData.caseID;
    }
    console.log ("getCommonReport:URL:"+urlScript);

    request({
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Cookie': cookie
        },
        uri: urlScript,
        method: "GET"
    }, function (err, res, body) {
        console.log (body);
        var $ = cheerio.load(body);
        var action = $("form").first().attr("action");
        var ar  = action.split("/");
        action = ar[ar.length-1];
        console.log("Action path:"+action);
        inres.json({formAction:action});
    });


};

function getPersonsContent(inreq, inres, urlScript,pageName,pageDetails,formData,contentLength) {


    console.log ("getPersonsContent entered");
    request({
        headers: {
            'Content-Length': contentLength,
            'Content-Type': 'application/x-www-form-urlencoded',
            'Cookie': cookie
        },
        uri: urlScript,
        method: 'POST',
        body: formData
    }, function (err, res, body) {
        var options1 = [];
        var options2 = [];
        console.log("url:"+url+" err:"+err);

        var $ = cheerio.load(body);

        console.log("body:"+ $("html").html());

        var table = [];
        var pageHeading = [];

        var h2 =  $("h2").first().text();
        var h4 = $("h4").first().text();
        pageHeading.push([h2,h4]);


        $("center").first().find("tr").each(function (index) {
            tds = [];
            tdo = $(this);
            tdo.find("td[valign='CENTER'],td[valign='center']").each(function (index) {
                var tdnode = $(this);
                var anode = tdnode.find("a");
                val = tdnode.text().trim();
                if (anode.length == 1) {
                    tds.push(anode.attr("href"));
                    tds.push(anode.text());
                }
                else if (val != '') {
                    tds.push(val);
                }
            });


            table.push(tds);

        });
        var billing = getBilling($);

        var returnDoc = {};
        returnDoc[pageName] = pageHeading;
        returnDoc[pageDetails] = table;
        returnDoc['billing'] = billing;

        inres.json(returnDoc);
    });
}

    exports.loginAndSetCookie = loginAndSetCookie;
    exports.getCookie = getCookie;
    exports.getMainQuery = getMainQuery;
    exports.getAttorney=getAttorney;
    exports.getParty=getParty;
    exports.getCaseSummary=getCaseSummary;
    exports.getFilers=getFilers;
    exports.getDocketReport=getDocketReport;
    exports.getCriminalCaseReport=getCriminalCaseReport;
    exports.getDocketActivityReport=getDocketActivityReport;
    exports.getPersons=getPersons;
    exports.getCaseNum=getCaseNum;
    exports.getFormAction=getFormAction;
    exports.getCriminalCaseReportQuery=getCriminalCaseReportQuery;
    exports.getCivilCaseReportQuery=getCivilCaseReportQuery;
    exports.getCalendarEventsReportQuery = getCalendarEventsReportQuery;
    exports.getDocketActivityReportQuery = getDocketActivityReportQuery;
    exports.getWrittenOptionsReportQuery = getWrittenOptionsReportQuery;
    exports.getJudgmentIndexReportQuery = getJudgmentIndexReportQuery;
    exports.getReport=getReport;
    exports.getFormOptions = getFormOptions;
    exports.getCommonFormData=getCommonFormData;
    exports.getMainData = getMainData;
